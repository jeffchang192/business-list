package io.jeffchang.businesslist.ui.business.di.module

import dagger.Module
import dagger.Provides
import io.jeffchang.businesslist.ui.business.data.service.BusinessService
import io.jeffchang.businesslist.ui.business.repository.BusinessRepository
import io.jeffchang.businesslist.ui.business.repository.DefaultBusinessRepository
import io.jeffchang.businesslist.ui.business.usecase.DefaultGetBusinessUseCase
import io.jeffchang.businesslist.ui.business.usecase.GetBusinessUseCase
import io.jeffchang.core.scope.FeatureScope
import retrofit2.Retrofit

@Module
class BusinessDataModule {
    @Provides
    @FeatureScope
    fun provideBusinessService(retrofit: Retrofit): BusinessService =
        retrofit.create(BusinessService::class.java)

    @Provides
    @FeatureScope
    fun provideBusinessRepository(
        businessService: BusinessService
    ): BusinessRepository =
        DefaultBusinessRepository(businessService)

    @Provides
    @FeatureScope
    fun provideGetBusinessUseCase(
        BusinessRepository: BusinessRepository
    ): GetBusinessUseCase = DefaultGetBusinessUseCase(BusinessRepository)

}
