package io.jeffchang.businesslist.ui.business.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import io.jeffchang.businesslist.ui.business.usecase.GetBusinessUseCase
import io.jeffchang.core.ContextProvider
import io.jeffchang.core.data.ViewState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext


class BusinessViewModel @Inject constructor(
    private val contextProvider: ContextProvider,
    private val getBusinessUseCase: GetBusinessUseCase
) : ViewModel() {

    private var searchJob: Job? = null

    private val viewState = MutableLiveData<PagingData<Business>>()

    private var currentQueryValue: String? = null

    private var currentSearchResult: Flow<PagingData<Business>>? = null

    fun viewState(): LiveData<PagingData<Business>> = viewState

    fun search(query: String) {
        // Make sure we cancel the previous job before creating a new one
        searchJob?.cancel()
        searchJob = launch {
            getBusinesses(query)?.collectLatest {
                viewState.postValue(it)
            }
        }
    }

    private fun getBusinesses(queryString: String? = currentQueryValue): Flow<PagingData<Business>>? {
        if (queryString == null) return null
        val lastResult = currentSearchResult
        if (queryString == currentQueryValue && lastResult != null) {
            return lastResult
        }
        currentQueryValue = queryString
        val newResult = getBusinessUseCase(queryString)
            .cachedIn(viewModelScope) // prevents having to call again on lifecycle recreation.
        currentSearchResult = newResult
        return newResult
    }

    private fun launch(
        coroutineContext: CoroutineContext = contextProvider.main,
        block: suspend CoroutineScope.() -> Unit
    ): Job {
        return viewModelScope.launch(coroutineContext) { block() }
    }
}
