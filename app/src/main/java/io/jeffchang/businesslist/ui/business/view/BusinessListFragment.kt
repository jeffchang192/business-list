package io.jeffchang.businesslist.ui.business.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import io.jeffchang.businesslist.R
import io.jeffchang.businesslist.databinding.FragmentBusinessListBinding
import io.jeffchang.businesslist.ui.business.inject
import io.jeffchang.businesslist.ui.business.view.adapter.BusinessListAdapter
import io.jeffchang.businesslist.ui.business.viewmodel.BusinessViewModel
import io.jeffchang.core.KnownNetworkException
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val LAST_SEARCH_QUERY = "LAST_SEARCH_QUERY"

private const val DEFAULT_QUERY = "Ramen"

class BusinessListFragment : Fragment() {

    private lateinit var binding: FragmentBusinessListBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    // Kotlin delegate to lazily create viewmodels.
    private val businessViewModel by viewModels<BusinessViewModel> {
        viewModelFactory
    }

    private val businessListAdapter by lazy {
        BusinessListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inject()
        binding = FragmentBusinessListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            businessListRecyclerView.adapter = businessListAdapter
            businessListRecyclerView.layoutManager = LinearLayoutManager(context)
            swipeRefreshLayout.setOnRefreshListener {
                businessViewModel.search(searchView.query.trim().toString())
            }
            val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            businessListRecyclerView.addItemDecoration(decoration)

        }
        initSearch(savedInstanceState)
        subscribeUI()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, binding.searchView.query.trim().toString())
    }

    private fun subscribeUI() {
        binding.apply {
            businessListAdapter.addLoadStateListener { loadState ->
                binding.apply {
                    if (loadState.source.refresh is LoadState.NotLoading) {
                        businessListRecyclerView.isVisible = true
                        if (businessListAdapter.itemCount == 0) {
                            Toast.makeText(
                                context,
                                R.string.empty_list,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                    // Show loading spinner during initial load or refresh.
                    progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                    // Show the retry state if initial load or refresh fails.
                    if (loadState.source.refresh is LoadState.Error) {
                        Toast.makeText(context, R.string.network_error, Toast.LENGTH_LONG).show()
                    }

                    val errorState = loadState.source.append as? LoadState.Error
                        ?: loadState.source.prepend as? LoadState.Error
                        ?: loadState.append as? LoadState.Error
                        ?: loadState.prepend as? LoadState.Error
                    errorState?.let {
                        // not pretty but avoids the issues when exceeding the API limit
                        if ((it.error as? KnownNetworkException)?.errorCode == 400) {
                            return@addLoadStateListener
                        }
                        Toast.makeText(
                            context,
                            R.string.network_error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }

            businessViewModel.viewState().observe(viewLifecycleOwner, Observer {
                binding.swipeRefreshLayout.isRefreshing = false
                lifecycleScope.launch {
                    businessListAdapter.submitData(it)
                }
            })
        }
    }

    private fun initSearch(savedInstanceState: Bundle?) {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrBlank()) {
                    return false
                }
                businessViewModel.search(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // no-op
                return false
            }
        })

        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY)
        binding.searchView.setQuery(query ?: DEFAULT_QUERY, query == null)

        // Scroll to top when the list is refreshed from network.
        lifecycleScope.launch {
            businessListAdapter.loadStateFlow
                // Only emit when REFRESH LoadState changes.
                .distinctUntilChangedBy { it.refresh }
                // Only react to cases where REFRESH completes i.e., NotLoading.
                .filter { it.refresh is LoadState.NotLoading }
                .collect { binding.businessListRecyclerView.scrollToPosition(0) }
        }
    }
}
