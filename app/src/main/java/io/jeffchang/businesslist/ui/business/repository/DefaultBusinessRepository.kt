package io.jeffchang.businesslist.ui.business.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import io.jeffchang.businesslist.ui.business.data.service.BusinessService
import kotlinx.coroutines.flow.Flow

private const val YELP_PAGE_SIZE = 50

class DefaultBusinessRepository(private val businessService: BusinessService) : BusinessRepository {

    override fun getBusinesses(term: String): Flow<PagingData<Business>> =
        Pager(
            config = PagingConfig(
                pageSize = YELP_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { YelpBusinessPagingSource(term, businessService) }
        ).flow
}