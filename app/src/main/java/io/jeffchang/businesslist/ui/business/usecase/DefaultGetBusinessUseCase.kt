package io.jeffchang.businesslist.ui.business.usecase

import androidx.paging.PagingData
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import io.jeffchang.businesslist.ui.business.repository.BusinessRepository
import kotlinx.coroutines.flow.Flow

class DefaultGetBusinessUseCase(
    private val businessRepository: BusinessRepository
) : GetBusinessUseCase {

    override fun invoke(term: String): Flow<PagingData<Business>> {
        return businessRepository.getBusinesses(term)
    }

}