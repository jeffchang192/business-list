package io.jeffchang.businesslist.ui.business.repository

import androidx.paging.PagingSource
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import io.jeffchang.businesslist.ui.business.data.service.BusinessService
import io.jeffchang.core.safePagedApiCall

private const val YELP_STARTING_IDX = 0

private const val YELP_PAGE_SIZE = 50

class YelpBusinessPagingSource(
    private var query: String = "",
    private val businessService: BusinessService
) : PagingSource<Int, Business>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Business> {
        return safePagedApiCall {
            val offset = params.key ?: 0
            val results = businessService.getBusinesses(query, offset)
            // Builds snapshots of what the next and previous page parameters should entail.
            val previous = if (offset == YELP_STARTING_IDX) {
                offset - YELP_PAGE_SIZE
            } else {
                null
            }
            val next = if (results.businesses.isNotEmpty()) {
                offset + YELP_PAGE_SIZE
            } else {
                null
            }
            LoadResult.Page(
                data = results.businesses,
                prevKey = previous,
                nextKey = next
            )
        }
    }
}