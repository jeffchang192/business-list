package io.jeffchang.businesslist.ui.business.repository

import androidx.paging.PagingData
import io.jeffchang.core.Result
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import kotlinx.coroutines.flow.Flow


interface BusinessRepository {

    fun getBusinesses(term: String): Flow<PagingData<Business>>

}
