package io.jeffchang.businesslist.ui.business.usecase

import androidx.paging.PagingData
import io.jeffchang.core.Result
import io.jeffchang.businesslist.ui.business.data.model.business.Business
import kotlinx.coroutines.flow.Flow


interface GetBusinessUseCase {

    operator fun invoke(term: String): Flow<PagingData<Business>>

}