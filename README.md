## Weedmaps Mobile Project

**Build Instructions**

* Clone Project and Use at the minimum Android Studio 4.0

- Build in Android Studio. Should require nothing more out of the box.

### Setbacks
I decided to use an alpha version of the Paging component provided by Google. It was a call I decided on because the stakes are low and this wouldn't be consumed by real users. Unfortunately, some of the API surface for v3 of the Paging Component is flaky. When I was researching how to write Unit Tests, I could only find hacky and time consuming methods that ultimately did not capture strictly business logic well and is difficult to maintain. That's why I opted to not bother and commented out the Unit Tests. The issue largely is that a lot of the error handling and behavior is buried within the PagingData class itself. However, I had unit tests before incorporating the paging library for demonstration knowledge of testing. Can find in commit [98ef4852](https://gitlab.com/jeffchang192/business-list/-/tree/98ef4852dc4fa16b22f451628a7f5a7e5dd77885)

### Features
* Adds infinite scrolling to continue paging Yelp Fusion's API.
* Use Platform's Search bar to narrow search terms.

### Your Focus Areas
Software Architecture, Code Style, Readability, Robustness and using standard Android development practices.

* Unit Tests
* Modularization
* Kotlin gradle buildscripts
* Tested with Rotation
* Coroutines

### Copied-in code or copied-in dependencies
I took some code for the DI setup, the error-handling abstractions and dependencies from past projects on a private repo. The idea for the domain mapping came from [this project](https://github.com/cobeisfresh/CleanArchitecture-Android-Showcase).

### What's Next
* Create more views for empty, different error states, and no internet states. I opted to use Toasts for the sake of time
* Optimizing and creating different layouts for landscape or tablet configurations.

### Screenshots
<table>
    <tr>
        <td><img style="width: 350px: height:auto; margin: 0 50px" src="screenshots/horizontal.png"></img></td>
        <td><img src="screenshots/vertical.png"></img></td>
    </tr>
    <tr>
        <td align="center"><b>Horizontal</b></img></td>
        <td align="center"><b>Vertical</b></img></td>
    </tr>
</table>